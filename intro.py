import math
import matplotlib.pyplot as plt


def gen_times(dt, length):
    ts = []
    t = 0
    while True:
        ts.append(t)
        t += dt
        if t >= length:
            break
    return ts


def harmonic(ts, f):
    xs = []
    for t in ts:
        x = math.cos(2 * math.pi * f * t)
        xs.append(x)
    return xs


def main():
    ts = gen_times(1/13, 10)
    xs = harmonic(ts, 10)
    plt.plot(ts, xs, "ro")

    ts = gen_times(1/100, 10)
    xs = harmonic(ts, 10)
    plt.plot(ts, xs)
    plt.show()


if __name__ == "__main__":
    main()
