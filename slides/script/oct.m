function [t,x] = demo_generate(f,T,A=1,phi=0,dt=0.5)
	t = [0:dt:T];
	x = A*cos(2*pi*f*t+phi);
endfunction

function test_demo_generate()
	[t,x]=demo_generate(0.05,200);
	subplot(3,1,1);
		plot(t,x);
	[t,x]=demo_generate(0.05,200,3);
	subplot(3,1,2);
		plot(t,x);
	[t,x]=demo_generate(0.05,200,3,10);
	subplot(3,1,3);
		plot(t,x);
	print("~/generate.png","-S700,500");
endfunction

function [f,X] = demo_spectra(t,x)
	X = abs(fft(x));
	dt = t(2)-t(1);
	f_max = 1/dt;
	df = f_max/length(X);
	f = [0:df:f_max-df];
endfunction

function test_demo_spectra()
	[t,x1] = demo_generate(0.2,200,3);
	[t,x2] = demo_generate(0.4,200,2);
	[t,x3] = demo_generate(0.6,200,1);
	[t,x4] = demo_generate(0.8,200,1);
	x = x1 .+ x2 .+ x3 .+ x4;
	subplot(2,1,1);
		plot(t,x);
	[f,X] = demo_spectra(t,x);
	subplot(2,1,2);
		plot(f,X);
	print("~/spectra.png","-S700,500");
endfunction

function test_demo_spectra2()
	[t,x1] = demo_generate(0.05,100,3);
	[t,x2] = demo_generate(0.7,100,0.5);
	x = x1 .+ x2;
	subplot(2,1,1);
		plot(t,x);
	[f,X] = demo_spectra(t,x);
	subplot(2,1,2);
		plot(f,X);
	print("../pic/spectra2.png","-S1000,700");
endfunction

function test_demo_fourSpectra()
	[t,x] = demo_generate(0.05,100);
	subplot(2,2,1);
		plot(t,x);
	[f,X] = demo_spectra(t,x);
	subplot(2,2,2);
		plot(f,X);
	[t,x] = demo_generate(0.2,100);
	subplot(2,2,3);
		plot(t,x);
	[f,X] = demo_spectra(t,x);
	subplot(2,2,4);
		plot(f,X);
	print("~/fourSpectra.png","-S1000,700");
endfunction

function picNoLabels()
	[t,x] = demo_generate(0.2,100);
	[f,X] = demo_spectra(t,x);
	plot(f,X);
	xlabel("frequency");
	axis("labelx");
	print("~/noLabels.png","-S1000,700");
endfunction

# wavelet transform
function w = demo_generateWavelet(t)
	mu = (t(end)-t(1))/2;
	sigma = mu/3;
	w = (mu-t) .* exp(-(t-mu).^2 / (2*sigma^2));
endfunction

function CW = demo_WT(t,x,s)
	CW = repmat(NaN,length(s),length(x));
	for i=1:1:length(s)
		tw = [0:t(2)-t(1):t(s(i))-t(1)];
		w = demo_generateWavelet(tw);
		row = conv(x,w)/(s(i)^0.5);
		l = length(w);
		CW(i,fix(l/2):end-round(l/2)-1) = row(l:end-l);
	end
endfunction

function test_demo_WT()
	s = [2:1:100];
	[t,x] = demo_generate(0.05,200);
	subplot(2,1,1);
		plot(t,x);
	CW = abs(demo_WT(t,x,s));
	subplot(2,1,2);
		imagesc([t(1),t(end)],[s(1),s(end)],CW);
	print("~/WT.png","-S1000,700");
endfunction

function x = myline(t,tend)
	x = 1 - t/tend;
endfunction

function demo_gibbs
	f1 = 0.03; f2 = 0.22;
	f = [0:0.001:0.25];
	subplot(1,4,1)
		plot(f,((f>=f1)&(f<=f2)));
		axis([f(1),f(end),-0.2,1.2]);

	w1=2*pi*f1; w2=2*pi*f2;
	dt = 0.5;
	l = 300;
	tloc = [-dt*l:dt:dt*l];
	operator = (sin(w2*tloc)-sin(w1*tloc))./tloc;
	operator(l+1) = (operator(l)+operator(l+2))/2;
	# calibrating
	tc = [0:dt:(l*4-1)*dt];
	hc = (f1+f2)/2;
	xc = cos(2*pi*hc*tc);
	xfc = conv(xc,operator);
	koeff = max(xfc(3*l:5*l));
	operator = operator./koeff;
	subplot(1,4,2)
		plot(tloc,operator);
	
	[f,H1] = demo_spectra(tloc,operator);
	subplot(1,4,3)
		plot(f,H1);
		axis([f(f>=0)(1),f(f>=0.25)(1),-0.2,1.2]);

	# smoothing
	wnd1 = 1-tloc(tloc<0)/(-l/2);
	wnd2 = 1-tloc(tloc>=0)/(l/2);
	wnd = wnd1;
	wnd(end+1:end+length(wnd2)) = wnd2;
	operator = operator.*wnd;
	[f,H1] = demo_spectra(tloc,operator);
	subplot(1,4,4)
		plot(f,H1);
		axis([f(f>=0)(1),f(f>=0.25)(1),-0.2,1.2]);

	print("../pic/gibbs.png","-S1700,300");
endfunction
