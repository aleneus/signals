import scipy as sp
import scipy.signal as spsig
import numpy as np
import pylab as plt

time = 1
df = 500

def gsig(A, h, t, phase_shift=0):
	return A*np.cos(2 * np.pi * h * t + phase_shift) 

t = np.linspace(0, time, time*df)
x_noise = gsig(1, 20.0, t, np.pi / 4.0) \
		+ gsig(1, 3.0, t, np.pi) \
		+ gsig(1, 10.0, t, 1.0) \

x_noise += np.random.normal(0, 0.3, time*df)

x_smooth = spsig.convolve(x_noise, spsig.hanning(30), 'same')

plt.subplot('211')
plt.title('Source signal')
plt.plot(t, x_noise)

plt.subplot('212')
plt.title('Smoothed signal')
plt.plot(t, x_smooth)

plt.show()
