import matplotlib.pyplot as plt
import numpy as np
from pylab import rcParams

def harmonic(T, dt, f, phi = 0, A = 1):
    t = np.linspace(0, T, T/dt + 1)
    x = A* np.cos(2 * np.pi * f * t + phi)
    return (t, x)


rcParams['figure.figsize'] = 12, 5

plt.figure()
t, x = harmonic(20*60, 0.5, 0.05)
tw, xw = harmonic(20*60, 17, 0.05)
plt.plot(t, x, linewidth = 1)
plt.plot(tw, xw, 'o', tw, xw, linewidth = 2)
plt.xlim(t[0],t[-1])
plt.grid(True)
plt.title("f = 0.05 Hz, Sampling frequency = 0.059 Hz ")
plt.xlabel("Time [sec]")
plt.ylabel("Amplitude")
plt.savefig('../about-aliasing-17.png')

plt.figure()
t, x = harmonic(20*60, 0.5, 0.05)
tw, xw = harmonic(20*60, 13, 0.05)
plt.plot(t, x, linewidth = 1)
plt.plot(tw, xw, 'o')
plt.xlim(t[0],t[-1])
plt.grid(True)
plt.title("f = 0.05 Hz, Sampling frequency = 0.077 sec")
plt.xlabel("Time [sec]")
plt.ylabel("Amplitude")
plt.savefig('../about-aliasing-13.png')
